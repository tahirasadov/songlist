import React, { Component }             from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import uuid                             from "uuid";

import SongInfo  from './components/SongInfo';
import Delete    from './components/Delete';
import Header    from './components/Header';
import Songs     from './components/Songs';
import Edit      from './components/Edit';
import Add       from './components/Add';

class App extends Component {

  state = {
    songs: [{
      id:     'uid1',
      artist: 'Jasper De Ceuster',
      title:  'Elements',
      genre:  'Electronics'
    },
    {
      id:     'uid2',
      artist: 'A.L.I.S.O.N',
      title:  'Motif',
      genre:  'Electronics'
    },
    {
      id:     'uid3',
      artist: 'Florida Skyline',
      title:  'Blueberry',
      genre:  'Electronics'
    }]
  }

  addSong = (song) => {
    this.setState({
      ...this.state,
      songs: [...this.state.songs, song]
    });
  }

  updateSong = (songs) => {
    this.setState({
      ...this.state,
      songs: songs
    });
  }

  deleteSong = (id) => {
    const songs = this.state.songs.filter((song) => {
        return id !== song.id;
    });
    this.updateSong(songs);
  }
  
  render(){
    return (
      <div className="App container">
          <BrowserRouter>
            <Header />
            <Switch>
              <Route exact path="/" component={() => <Songs songs={this.state.songs} />} />

              <Route exact path="/add" component={() => <Add addSong={this.addSong} />} />

              <Route exact path="/edit/:id(\w+)" component={() => <Edit updateSong={this.updateSong} songs={this.state.songs} />} />
              
              <Route exact path="/delete/:id(\w+)" component={() => <Delete updateSong={this.updateSong} deleteSong={this.deleteSong} songs={this.state.songs} />} />

              <Route exact path="/song/:id(\w+)" component={() => <SongInfo songs={this.state.songs}/>} />
            </Switch>
          </BrowserRouter>
      </div>
    );
  }

}

export default App;
