import React, { Component } from 'react';

import { NavLink }          from "react-router-dom";

class Header extends Component {
    render(){
        return (
            <nav className="cyan">
                <div className="nav-wrapper">
                    <NavLink to="/" className="brand-logo left">Logo</NavLink>
                    <ul id="nav-mobile" className="right">
                        <li>
                            <NavLink to="/">Songs</NavLink>
                        </li>
                        <li>
                            <NavLink to="/add">New Song</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }

}

export default Header;