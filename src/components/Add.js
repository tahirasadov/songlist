import React, { Component } from 'react';
import { withRouter }       from 'react-router-dom';
import uuid                 from "uuid";

class Add extends Component {

    constructor(props){
        super(props);
        this.state = {
            id:     uuid.v1().replace(/-/g,""),
            artist: null,
            title:  null,
            genre:  null
        }
    }

    formSubmit = () => {
        this.props.addSong(this.state);
        this.props.history.push('/');
    }

    updateState = (e) => {
        let id = e.target.id;
        this.setState({
            [id]: e.target.value,
        });
    }

    render(){
        return(
            <div className="container">
                <div className="row">
                    <h5>Add new song</h5>
                    <div className="input-field col s12">
                        <input id="artist" onChange={this.updateState} type="text" />
                        <label className="active" htmlFor="artist">Artist name</label>
                    </div>
                    <div className="input-field col s12">
                        <input id="title" onChange={this.updateState} type="text" />
                        <label className="active" htmlFor="title">Song name</label>
                    </div>
                    <div className="input-field col s12">
                        <input id="genre" onChange={this.updateState} type="text" />
                        <label className="active" htmlFor="genre">Genre</label>
                    </div>
                </div>
                <div className="row">
                    <button className="btn right" onClick={() => {this.formSubmit()}}>Add <i className="material-icons right">save</i></button>
                </div>
            </div>
        );
    }
}

export default withRouter(Add);