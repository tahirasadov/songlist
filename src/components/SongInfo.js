import React, { Component } from 'react';
import { withRouter }       from 'react-router-dom';

class SongInfo extends Component {
    constructor(props){
        super(props);

        const singleSong = this.props.songs.filter((song) => {
            let paramid    = this.props.match.params.id;
            return paramid === song.id;
        });
        this.state = singleSong[0]
        
    }

    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <h5>Song info</h5>
                        <div><b>Artist: </b>{this.state.artist}</div>
                        <div><b>Title: </b>{this.state.title}</div>
                        <div><b>Genre: </b>{this.state.title}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(SongInfo);