import React, { Component } from 'react';
import { withRouter }       from 'react-router-dom';

class Delete extends Component {
    constructor(props){
        super(props);

        const singleSong = this.props.songs.filter((song) => {
            let paramid    = this.props.match.params.id;
            return paramid === song.id;
        });
        this.state = singleSong[0]
    }

    dontDeleteSong = () => {
        this.props.history.push('/');
    }

    deleteSong = () => {
        this.props.deleteSong(this.state.id);
        this.props.history.push('/');
    }

    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col s12">
                    <h5>Are you sure delete "{this.state.title}" song?</h5>
                    <br />
                    <a onClick={this.dontDeleteSong} className="btn deep-orange accent-4 waves-effect right col s2 offset-s1">No</a>
                    <a onClick={this.deleteSong}  className="btn deep-orange accent-4 waves-effect right col s2 offset-s1">Yes</a>
                    </div>
                </div>
            </div>
        );
    }

}

export default withRouter(Delete);