import React, { Component } from 'react';
import { withRouter }       from 'react-router-dom';
import uuid                 from "uuid";

class Edit extends Component {

    constructor(props){
        super(props);
        const singleSong = this.props.songs.filter((song) => {
            let paramid    = this.props.match.params.id;
            return paramid === song.id;
        });
        this.state = singleSong[0]
    }

    formSubmit = () => {
        let songs = Object.assign([], this.props.songs);
        let newsongs = songs.map(song => {
            if(song.id == this.state.id){
                return this.state;
            }
            return song;
        });
        this.props.updateSong(newsongs);
        this.props.history.push('/');
    }

    updateState = (e) => {
        let id = e.target.id;
        this.setState({
            [id]: e.target.value,
        });
    }
    
    render(){
        return(
            <div className="container">
                <div className="row">
                    <h5>Edit song</h5>
                    <div className="input-field col s12">
                        <input value={this.state.artist} id="artist" onChange={this.updateState} type="text" />
                        <label className="active" htmlFor="artist">Artist name</label>
                    </div>
                    <div className="input-field col s12">
                        <input value={this.state.title} id="title" onChange={this.updateState} type="text" />
                        <label className="active" htmlFor="title">Song name</label>
                    </div>
                    <div className="input-field col s12">
                        <input value={this.state.genre} id="genre" onChange={this.updateState} type="text" />
                        <label className="active" htmlFor="genre">Genre</label>
                    </div>
                </div>
                <div className="row">
                    <button className="btn right" onClick={() => {this.formSubmit()}}>Update <i className="material-icons right">save</i></button>
                </div>
            </div>
        );
    }
}

export default withRouter(Edit);
