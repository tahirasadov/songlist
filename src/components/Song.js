import React, { Component } from 'react';
import { NavLink }          from "react-router-dom";

class Song extends Component {
    render(){
        return(
            <li className="collection-item">
              <NavLink className="blue-grey-text darken-4" to={{
                pathname: "/song/" + this.props.id
              }}>{this.props.artist} - {this.props.title} - ({this.props.genre})</NavLink>
              <NavLink className="blue-grey-text darken-4" to={{
                pathname: "/delete/" + this.props.id
              }} className="secondary-content">
                <i className="deep-orange-text accent-3 material-icons">delete</i>
              </NavLink>
              <NavLink to={{
                pathname: "/edit/" + this.props.id
              }} className="secondary-content">
                <i className="blue-grey-text darken-4 material-icons">edit</i>
              </NavLink>
            </li>
        );
    }
}

export default Song;