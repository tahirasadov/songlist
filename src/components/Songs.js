import React, { Component } from 'react';

import Song                 from './Song';

class Songs extends Component {
    render(){
        const SongList = this.props.songs.map((song, id) => {
            return(<Song artist={song.artist} key={song.id} id={song.id} title={song.title} genre={song.genre}/>);
        });

        return(
        <ul className="collection">
            {SongList }
        </ul>
        );
    }
}

export default Songs;